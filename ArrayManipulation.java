public class ArrayManipulation {
    public static void main(String[] args)  {
        int[] numbers = {5,8,3,2,7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        Double[] valua = new Double[4];

        //Print the elements of the "numbers" array using a for loop.
        for (int i=0;i<5;i++){
            System.out.print(numbers[i]+" ");
        }
        System.out.println();

        //Print the elements of the "names" array using a for-each loop.
        for (int i=0;i<4;i++){
            System.out.print(names[i]+" ");
        } 
        
        //Initialize the elements of the "values" array with any four decimal values of your choice
        valua[0] = 1.1;
        valua[1] = 1.2;
        valua[2] = 1.3;
        valua[3] = 1.4;

        //Calculate and print the sum of all elements in the "numbers" array.
        int sum =0;
        for(int i = 0;i<5;i++){      
            sum = sum+ numbers[i];
        }
        System.out.println();
        System.out.println(sum);


        //Find and print the maximum value in the "values" array.
        double max = 0;
        for (int i=0;i<4;i++){         
            if (valua[i]>max){
                max = valua[i];
                
            }           
        }
        System.out.println(max);

        //Create a new string array named "reversedNames" with the same length as the "names" array.
        //Fill the "reversedNames" array with the elements of the "names" array in reverse order.
        //Print the elements of the "reversedNames" array.
        String[] reverseNames = new String[names.length];
        for(int i = 0;i<names.length;i++){      
            reverseNames[i]=names[3-i];
            System.out.println(reverseNames[i]);        
        }

        for(int i=0;i<5;i++){
            if (numbers[i]>numbers[i+1]){
                numbers[i]=numbers[i+1];             
            }
        }


        


    }
}
